using api_cadastrando.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Collections;
using System.Collections.Generic;
using api_cadastrando.ViewModels.RetornoDadosApiViewModels;

namespace api_cadastrando.ViewModels.UsuariosViewModels
{
    public class GetRoleUserViewModel : BaseRetornoViewModel
    {

        public int Id { get; set; }

        public string Email { get; set; }

        public string RoleUser { get; set; }

        public string Name { get; set; }




        public JsonResult ValidaDados(MyBaseController ctr)
        {
            // if (string.IsNullOrEmpty(Email))
            // {
            //     ctr.ModelState.AddModelError(nameof(Email), "Email está vazio");

            // }

            return null;
        }



        public JsonResult CarregaDados(MyBaseController ctr)
        {

            var roleUser = ctr.Db.Roles.Where(u => u.User.Id == Id).Include(u => u.User).FirstOrDefault();
            if (roleUser != null)
            {
                RoleUser = roleUser.Type;
                Email = roleUser.User.Email;
                Name = roleUser.User.Surname;
            }
            return null;
        }
    }
}