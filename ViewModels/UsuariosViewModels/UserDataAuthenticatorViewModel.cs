namespace api_cadastrando.ViewModels.UsuariosViewModels
{
    public class UserDataAuthenticatorViewModel
    {
        public string Token { get; set; }

        public string Email { get; set; }

        public string Role { get; set; }


    }
}