using System.ComponentModel.DataAnnotations;
using api_cadastrando.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Text.Json.Serialization;
using System.Collections.Generic;

namespace api_cadastrando.ViewModels.UsuariosViewModels
{
    public class CreateUserViewModel
    {
        [Required(ErrorMessage = "E-mail Obrigatatório")]
        public string Email { get; set; }

        public string Pass { get; set; }

        public bool HasCreated { get; set; }

        public string Msg { get; set; }

        public string PerfilUser { get; set; } = "client";


        public IEnumerable<string> Erros { get; set; }

        [Required(ErrorMessage = "Usuario Obrigatatório")]

        public string Usuario { get; set; }

        public JsonResult ValidationData(MyBaseController ctr)
        {

            if (!string.IsNullOrEmpty(Pass))
            {
                var db = ctr.Db;
                var userEmail = db.Users.AsNoTracking().FirstOrDefault(us => us.Email == Email.Trim());
                if (userEmail != null)
                {
                    ctr.ModelState.AddModelError(nameof(Email), "Usuario já existe");
                }

            }
            else
            {
                ctr.ModelState.AddModelError(nameof(Pass), "Senha vazia");
            }


            switch (PerfilUser)
            {
                case "admin":
                    PerfilUser = "admin";
                    break;
                case "client":
                    PerfilUser = "client";
                    break;
                default:
                    PerfilUser = "client";
                    break;
            }

            return null;
        }

    }
}