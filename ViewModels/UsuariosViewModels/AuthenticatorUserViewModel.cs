using System.ComponentModel.DataAnnotations;
using api_cadastrando.Controllers;
using api_cadastrando.ViewModels.RetornoDadosApiViewModels;
using Microsoft.AspNetCore.Mvc;

namespace api_cadastrando.ViewModels.UsuariosViewModels
{
    public class AuthenticatorUserViewModel : BaseRetornoViewModel
    {

        [Required(ErrorMessage = "E-mail Obrigatatório")]

        public string Email { get; set; }

        [Required(ErrorMessage = "Senha Obrigatatório")]
        public string Password { get; set; }

        public string Role { get; set; }

        public string Token { get; set; }

        public int Id { get; set; }




        public JsonResult ValidaCampos(MyBaseController ctr)
        {

            if (string.IsNullOrEmpty(Email) || string.IsNullOrEmpty(Password))
            {
                ctr.ModelState.AddModelError("paramentro", "Email ou senha vazio" );
            }



            return null;
        }

        

    }
}