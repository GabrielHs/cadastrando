using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using api_cadastrando.Controllers;
using api_cadastrando.ViewModels.RetornoDadosApiViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace api_cadastrando.ViewModels.ProdutosViewsModels
{
    public class CreateProductViewModel : BaseRetornoViewModel
    {
        [Required(ErrorMessage="Nome é obrigatorio")]
        public string Nome { get; set; }

        [Required(ErrorMessage="Preço é obrigatorio")]
        public decimal Preco { get; set; }

        [Required(ErrorMessage="Quantidade do estoque é obrigatorio")]
        public int QuantidadeEstoque { get; set; }

        public string DescricaoProduto { get; set; }

        public int IdUser { get; set; }

        [JsonIgnore]
        public IFormFile arquivo  { get; set; }





        public JsonResult ValidaCampos(MyBaseController ctr)
        {
            if (string.IsNullOrEmpty(Nome))
            {
                ctr.ModelState.AddModelError(nameof(Nome), "Nome está vazio");
            }

            var user = ctr.Db.Users.FirstOrDefault(user => user.Id == IdUser);

            if (user == null)
            {
                ctr.ModelState.AddModelError(nameof(IdUser), "Não encontrado usuário");
                
            }

            var product = ctr.Db.Produtcs.FirstOrDefault(p => p.Name == Nome);

            if (product != null)
            {
                ctr.ModelState.AddModelError(nameof(Nome), "Nome de produto já cadastrado");
                
            }


            return null;
        }


    }
}