namespace api_cadastrando.ViewModels.ErrosViewModels
{
    public class GenericErrorPageViewModel
    {
        public int? HttpErrorCode { get; set; }

        public string Nome { get; set; }

        public string Mensagem { get; set; }
    }
}