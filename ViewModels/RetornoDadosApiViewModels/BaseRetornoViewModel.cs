using System.Collections.Generic;

namespace api_cadastrando.ViewModels.RetornoDadosApiViewModels
{
    public class BaseRetornoViewModel
    {
        public bool Sucesso { get; set; }

        public IEnumerable<string> Erros { get; set; }

    }
}