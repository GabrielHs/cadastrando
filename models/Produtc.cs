using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace api_cadastrando.models
{
    public class Produtc
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(80)]
        public string Name { get; set; }

        // [Range(1, 100)]
        // [DataType(DataType.Currency)]
        [Required]
        [Column(TypeName = "decimal(5, 2)")]
        public decimal Price { get; set; }

        [Required]
        public string BarCode { get; set; }

        [Required]
        public int Quantity { get; set; }

        [MaxLength(150)]
        public string Describe { get; set; }

        [MaxLength(250)]
        public string PathImage { get; set; }

        public User User { get; set; }




    }
}