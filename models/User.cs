using System.ComponentModel.DataAnnotations;

namespace api_cadastrando.models
{
    public class User
    {
        public int Id { get; set; }

        [Required]
        public string Email { get; set; }

        [MaxLength(10)]
        public string Surname { get; set; }

        [Required]
        public string Password { get; set; }

    }
}