using api_cadastrando.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace api_cadastrando.Controllers
{

    public class HomeController : MyBaseController
    {
        public HomeController(CadastrandoContext context) : base(context)
        {
        }

        // [Authorize]
        // [Route("")]

        public IActionResult Get()
        {
            return Json(new { ok = "api de cadastrando", version = "0.0.1" });

        }

    }
}