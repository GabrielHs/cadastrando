using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using api_cadastrando.Data;
using api_cadastrando.models;
using api_cadastrando.ViewModels.ProdutosViewsModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using X.PagedList;

namespace api_cadastrando.Controllers
{

    public class ProductsController : MyBaseController
    {
        IWebHostEnvironment _appEnvironment;
        public ProductsController(CadastrandoContext context, IWebHostEnvironment env) : base(context)
        {
            _appEnvironment = env;
        }

        public readonly CultureInfo ciEN = new CultureInfo("pt-BR");

        [Route("api/v1/prods/{action}")]
        [HttpPost]
        [DisableRequestSizeLimit]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Created([FromForm] CreateProductViewModel model)
        {
            model.ValidaCampos(this);
            if (ModelState.IsValid)
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {

                        var barCode = Guid.NewGuid();

                        string caminhoDestinoArquivo = _appEnvironment.WebRootPath + "\\" + "imgs\\products" + "\\";

                        string caminhoDestinoArquivoOriginal = caminhoDestinoArquivo + barCode.ToString() + model.arquivo.FileName;

                        string pathImage = $"/imgs/products/{barCode.ToString() + model.arquivo.FileName}";
                        if (model.arquivo.Length > 0)
                        {
                            using (var stream = new FileStream(caminhoDestinoArquivoOriginal, FileMode.Create))
                            {
                                await model.arquivo.CopyToAsync(stream);
                            }
                        }

                        var user = db.Users.FirstOrDefault(user => user.Id == model.IdUser);

                        model.DescricaoProduto = model.DescricaoProduto == null ? "Não informado" : model.DescricaoProduto;

                        decimal priceProduct = Convert.ToDecimal(model.Preco, ciEN);
                        Produtc tbProduct = new Produtc()
                        {
                            Name = model.Nome,
                            BarCode = barCode.ToString("N"),
                            Price = priceProduct,
                            Quantity = model.QuantidadeEstoque,
                            Describe = model.DescricaoProduto,
                            PathImage = pathImage,
                            User = user
                        };

                        db.Produtcs.Add(tbProduct);
                        db.SaveChanges();
                        transaction.Commit();

                        model.Sucesso = true;

                        return Json(model);

                    }
                    catch (System.Exception ex)
                    {
                        model.Sucesso = false;
                        model.Erros = new string[1] { "Erro ao cadastrar dados " + ex.InnerException };
                        transaction.Rollback();

                        return Json(model);

                    }
                }

            }
            else
            {
                model.Sucesso = false;
                model.Erros = ModelState.Values.SelectMany(err => err.Errors).Select(e => e.ErrorMessage);
                return Json(model);
            }


        }


        [Route("api/v1/produtos")]
        [HttpGet]
        [Authorize(Roles = "client, admin")]
        public IActionResult Index([FromQuery] int page = 1, [FromQuery] string somente = null)
        {

            if (somente != null)
            {
                var produtos = db.Produtcs
                                .Where(p => p.Name.Contains(somente) ||
                                 p.BarCode.Contains(somente) ||
                                 p.Id.ToString() == somente ||
                                 p.Price.ToString() == somente
                                 ).AsNoTracking()
                                 .Include(u => u.User);

                produtos.OrderBy(p => p.Name);

                var produtosPaginado = produtos.ToPagedList(page, 14);

                return Json(new { data = produtosPaginado, metadado = produtosPaginado.GetMetaData() });
            }
            else
            {
                var produtos = db.Produtcs.AsNoTracking().Include(u => u.User);

                produtos.OrderBy(p => p.Name);

                var produtosPaginado = produtos.ToPagedList(page, 14);
                return Json(new { data = produtosPaginado, metadado = produtosPaginado.GetMetaData() });
            }

        }



    }
}