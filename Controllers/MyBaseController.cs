using api_cadastrando.Data;
using Microsoft.AspNetCore.Mvc;

namespace api_cadastrando.Controllers
{
    public class MyBaseController : Controller
    {

        protected readonly CadastrandoContext db;

        public CadastrandoContext Db => db;


        public MyBaseController(CadastrandoContext context)
        {
            this.db = context;
        }

    }
}