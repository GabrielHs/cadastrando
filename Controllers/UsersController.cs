

using System.Linq;
using api_cadastrando.Data;
using api_cadastrando.Helpers;
using api_cadastrando.models;
using api_cadastrando.ViewModels.UsuariosViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace api_cadastrando.Controllers
{
    public class UsersController : MyBaseController
    {
        public UsersController(CadastrandoContext context) : base(context)
        {
        }



        [Route("api/v1/{controllers}/{action}")]
        [HttpPost]
        [AllowAnonymous]
        public IActionResult Created([FromBody] CreateUserViewModel model)
        {

            var errosNenhum = model.ValidationData(this);
            if (ModelState.IsValid)
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        var passwordHasher = new PasswordHasher<User>();
                        var user = new User();
                        var hashedPassword = passwordHasher.HashPassword(user, model.Pass);


                        User tbUsers = new User()
                        {
                            Email = model.Email,
                            Password = hashedPassword,
                            Surname = model.Usuario
                        };
                        db.Users.Add(tbUsers);
                        db.SaveChanges();

                        Role tbRoles = new Role()
                        {
                            Type = model.PerfilUser,
                            User = tbUsers
                        };

                        db.Roles.Add(tbRoles);
                        db.SaveChanges();

                        transaction.Commit();

                        model.HasCreated = true;
                        model.Msg = "Sucesso";
                        return Json(model);
                    }
                    catch (System.Exception ex)
                    {
                        model.HasCreated = false;
                        model.Msg = "Erro ao cadastrar dados " + ex.InnerException;
                        transaction.Rollback();


                        return Json(model);
                    }
                }


            }
            else
            {
                model.HasCreated = false;
                model.Msg = "Não foi possivel cadastrar dados";
                model.Erros = ModelState.Values.SelectMany(err => err.Errors).Select(e => e.ErrorMessage);
                return Json(model);
            }

        }

        [Route("api/v1/{controllers}/{action}/{id}")]
        [HttpGet]
        [Authorize]
        public IActionResult Papel([FromRoute] int id)
        {
            var model = new GetRoleUserViewModel();

            model.ValidaDados(this);

            if (ModelState.IsValid)
            {
                model.Id = id;
                model.CarregaDados(this);
                model.Sucesso = true;
                return Json(model);

            }

            model.Erros = ModelState.Values.SelectMany(e => e.Errors).Select(ers => ers.ErrorMessage);

            model.Sucesso = false;


            return Json(model);
        }


        [AllowAnonymous]
        [Route("api/v1/{controllers}/{action}")]
        [HttpPost]
        public IActionResult Autenticar([FromBody] AuthenticatorUserViewModel model)
        {

            model.ValidaCampos(this);

            if (ModelState.IsValid)
            {
                var user = db.Users.FirstOrDefault(us => us.Email == model.Email.Trim());

                if (user == null)
                {
                    model.Sucesso = false;
                    model.Erros = new string[1] { "Usuario não encontrado" };
                    return Json(model);

                }

                var passwordHasher = new PasswordHasher<User>();

                var hashedPassword = passwordHasher.VerifyHashedPassword(user, user.Password, model.Password);

                if (hashedPassword != PasswordVerificationResult.Success)
                {
                    model.Sucesso = false;
                    model.Erros = new string[1] { "Senha está incorreto" };
                    return Json(model);
                }

                var roleUser = db.Roles.FirstOrDefault(r => r.User.Id == user.Id);

                model.Role = roleUser.Type;

                var token = TokenService.GenerateToken(model);
                model.Token = token;
                model.Sucesso = true;
                model.Id = user.Id;
                return Json(model);
            }


            model.Sucesso = false;
            model.Erros = ModelState.Values.SelectMany(err => err.Errors).Select(e => e.ErrorMessage);
            return Json(model);

        }


    }
}