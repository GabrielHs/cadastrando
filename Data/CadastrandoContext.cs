using Microsoft.EntityFrameworkCore;
using api_cadastrando.models;



namespace api_cadastrando.Data
{
    public class CadastrandoContext : DbContext
    {
        public CadastrandoContext(DbContextOptions<CadastrandoContext> options) : base(options) { }

        public DbSet<User> Users { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Produtc> Produtcs { get; set; }

    }
}